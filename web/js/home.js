function mainIndex( urlLogin, urlChangeRating, isUserLoggedIn ) {
    var fileInput = document.getElementById("file-input");
    if ( fileInput ) {
        fileInput.onchange = function () {
            if (this.files[0]) {
                document.getElementById( 'selected-filename' ).value = this.files[0].name;
            } else {
                document.getElementById( 'selected-filename' ).value = '';
            }
        };
    }
    changeRatingOnClick( urlLogin, urlChangeRating );
    showThumbnailCaptionOnMouse();

    uploadImage( isUserLoggedIn );
   // showPackmanAnimation( 'hello_world' );
}

function showThumbnailCaptionOnMouse() {
    var thumbnails = document.getElementsByClassName('thumbnail');
    for (var i = 0; i < thumbnails.length; i++) {
        thumbnails[i].addEventListener( 'mouseover', function () {
            event.currentTarget.getElementsByClassName('my_thumb_caption')[0].style = 'display: block;';
        } )
        thumbnails[i].addEventListener( 'mouseout', function () {
            event.currentTarget.getElementsByClassName('my_thumb_caption')[0].style = 'display: none;';
        } )
        var h6 = thumbnails[i].getElementsByTagName( 'h6' )[0];
        if (h6) {
            h6.addEventListener( 'click', function () {
                event.currentTarget.parentElement.parentElement.getElementsByTagName( 'a' )[0].click();
            })
        }
    }
}

function changeRatingOnClick( urlLogin, urlChangeRating ) {
    var ratingBtns = document.getElementsByClassName('fa');
    for (var i = 0; i < ratingBtns.length; i++) {
        ratingBtns[i].addEventListener( 'click', changeRating )
        ratingBtns[i].urlChangeRating = urlChangeRating;
        ratingBtns[i].urlLogin = urlLogin;
    }
}

function changeRating() {
    var urlChangeRating = event.currentTarget.urlChangeRating;
    var urlLogin = event.currentTarget.urlLogin;
    var counter = event.currentTarget.parentNode.getElementsByClassName('image-rating')[0];
    var spinner = event.currentTarget.parentNode.getElementsByClassName('fa-spinner')[0];
    var imageId = event.currentTarget.parentNode.attributes['imageId'].value;
    var isLike = event.currentTarget.attributes['isLike'].value;
    var url = urlChangeRating + '?isLike=' + isLike + '&imageId=' + imageId;
    var xhr = new XMLHttpRequest();
    xhr.open( 'GET', url, true );

    spinner.style = 'display: inline-block';
    counter.textContent = '';
    xhr.send();

    xhr.onload = function () {
        switch ( xhr.status ) {
            case 200:
                var json = JSON.parse( xhr.responseText );
                counter.textContent = json.newRating;
                break;
            case 400:
                console.log(xhr.text);
                break;
            case 401:
                window.location.href = urlLogin;
                break;
            default:
                break;
        }
        spinner.style = 'display: none';
    }
}

function uploadImage( isUserLoggedIn ) {
    var uploading_msg = 'Die Datei wird hochgeladen. Bitte warten.';
    var converting_msg = 'Die Datei wird verarbeitet. Bitte warten.';

    var my_form_id = '#upload-form';
    var submit_btn = '#submit-upload';
    var progress_bar_id = '#progressbar';
    var result_output = '#status-field';
    var total_files_allowed = 1;
    var allowed_file_types = $( my_form_id ).find('input[type=file]')[0].accept.split(',');
    var max_file_size = $( my_form_id ).find('input[name=MAX_FILE_SIZE]')[0].value;
    var max_file_size_mb = max_file_size / 1024 / 1024;

    $( my_form_id ).on( "submit", function( event ) {
        event.preventDefault();
        var proceed = true; //set proceed flag
        var error = []; //errors
        var info = []; //info messages
        var selected_file_size = this.elements['pic'].files[0].size;
        var selected_file_size_mb = selected_file_size / 1024 / 1024;
        var selected_file_name = this.elements['pic'].files[0].name;
        var post_url = $(this).attr("action");

        var supported = typeof new XMLHttpRequest().responseType === 'string';

        if ( !( supported && window.File && window.FileReader && window.FileList && window.Blob) ){
            error.push( 'Your browser does not support new File Upload API! Please, upgrade' );
        } else {
            if(selected_file_size > max_file_size){
                error.push( "You have selected file with size " + Math.round(selected_file_size_mb) + " MB. Accepted size is " + max_file_size_mb +" MB, Try smaller file!");
                proceed = false; //set proceed flag to false
            }

            var regEx = /(?:\.([^.]+))?$/;
            var ext = regEx.exec( selected_file_name );
            if (typeof ext != undefined) {
                var ext = ext[0].toLowerCase();
            }
            if ( allowed_file_types.indexOf( ext ) == -1 ) {
                error.push( 'You have selected file with unaccepted extension' );
                proceed = false; //set proceed flag to false
            }

            var submit_btn  = $(this).find("input[type=submit]");

            if (proceed) {
                submit_btn.val("Please Wait...").prop( "disabled", true); //disable submit button
                var form_data = new FormData(this); //Creates new FormData object

                // showPackmanAnimation( uploading_msg );
                showProgressBar();
                document.getElementById( 'wait-uploading' ).getElementsByTagName( 'h3' )[0].innerHTML = uploading_msg;
                $.ajax({
                    url : post_url,
                    type: "POST",
                    data : form_data,
                    contentType: false,
                    cache: false,
                    processData:false,
                    xhr: function(){
                        //upload Progress
                        var xhr = $.ajaxSettings.xhr();
                        if (xhr.upload) {
                            xhr.upload.addEventListener('progress', function( event ) {
                                var percent = 0;
                                var position = event.loaded || event.position;
                                var total = event.total;
                                if ( event.lengthComputable ) {
                                    percent = Math.ceil(position / total * 100);
                                }
                                //update progressbar
                                $( progress_bar_id ).css( "width", + percent + "%" );
                                // $("progressbar.status").text(percent +"%");
                                if (percent >= 100) {
                                    // showPackmanAnimation( converting_msg );
                                    document.getElementById( 'wait-uploading' ).getElementsByTagName( 'h3' )[0].innerHTML = converting_msg;
                                }
                            }, true);
                        }
                        return xhr;
                    },
                    mimeType:"multipart/form-data",
                    error: function () {
                        $( result_output ).append(
                            '<div class="alert alert-danger">' +
                            'Error occured' +
                            '</div>');
                        $( my_form_id )[0].reset(); //reset form
                        $( submit_btn ).val("Upload").prop( "disabled", false); //enable submit button once ajax is done
                    }
                }).done( function( result ) {
                    hideProgressBar();

                    var response = JSON.parse( result );
                    switch (response.httpStatus) {
                        case 200:
                            $( result_output ).append(
                                '<div class="alert alert-success">' +
                                response.message + ' <a href="' +
                                response.redirectUrl + '">Click here to edit</a>' +
                                '</div>');
                            break;
                        case 301:
                            window.location.href = response.redirectUrl;
                            break;
                        case 401:
                            window.location.href = response.redirectUrl;
                            break;
                        case 500:
                            if ( response.isError ) {
                                $( result_output ).append(
                                    '<div class="alert alert-danger">' +
                                    response.message +
                                    '</div>');
                            } else {
                                $( result_output ).append(
                                    '<div class="alert alert-success">' +
                                    response.message +
                                    '</div>');
                            }

                            break;
                    }

                    $( my_form_id )[0].reset(); //reset form
                    $( submit_btn ).val("Upload").prop( "disabled", false); //enable submit button once ajax is done
                });
            }
        }

        $( result_output ).html( '' );
        $( error ).each( function( i ) { //output any error to output element
            $( result_output ).append('<div class="alert alert-danger">' + error[i] + "</div>");
        } );
        $( info ).each( function( i ) { //output any info to output element
            $( result_output ).append('<div class="alert alert-success">' + info[i] + "</div>");
        } );
    });
}

function showPackmanAnimation( msg_text ) {
    var h3 = document.getElementById( 'wait-uploading' ).getElementsByTagName( 'h3' )[0];
    //var isActive = ;
    var img = h3.getElementsByTagName( 'img' )[0];

    var h3Html = h3.innerHTML;
    var imgHtml = img.outerHTML;

   // while (true) {
   //
   // }
}

function showProgressBar() {
    document.getElementById( 'uploads' ).style.display = 'none';
    document.getElementById( 'wait-uploading' ).style.display = '';
}
function hideProgressBar() {
    document.getElementById( 'uploads' ).style.display = '';
    document.getElementById( 'wait-uploading' ).style.display = 'none';
}