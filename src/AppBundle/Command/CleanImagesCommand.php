<?php

namespace AppBundle\Command;

use AppBundle\Entity\Image;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CleanImagesCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('app:clean-images')
            ->setDescription('Removes Images without assigned users after 1 week.')
            ->setHelp("This command removes Images without assigned users after 1 week.");
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $uploads_dir = $this->getContainer()->getParameter('uploads_dir');
        $uploads_original_dir = $this->getContainer()->getParameter('uploads_original_dir');
        $entityManager = $this->getContainer()->get('doctrine.orm.entity_manager');

        $images = $this
            ->getContainer()
            ->get('doctrine')
            ->getRepository(Image::class)
            ->getOutdatedImages(604800);

        foreach ($images as $image) {
            if ($image->getUrl()) {
                unlink($uploads_dir . $image->getUrl());
            }
            if ($image->getOriginalUrl()) {
                unlink($uploads_original_dir . $image->getOriginalUrl());
            }

            $output->writeln('Image ' . $image->getId() . ' removed');
            $entityManager->remove($image);
        }
        $entityManager->flush();
    }
}