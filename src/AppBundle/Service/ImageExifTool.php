<?php

namespace AppBundle\Service;

use AppBundle\Entity\Image;

class ImageExifTool
{
    public $uploadsDir;
    public $uploadsOriginalDir;
    public $kernelRootDir;

    public function __construct($uploads_dir, $uploads_original_dir, $kernel_root_dir)
    {
        $this->uploadsDir = $uploads_dir;
        $this->uploadsOriginalDir = $uploads_original_dir;
        $this->kernelRootDir = $kernel_root_dir;
    }

    public function getExif(Image $image)
    {
        $result = '';
        $filename = $this->uploadsDir . $image->getUrl();
        if ($image->getOriginalUrl()) {
            $filename = $this->uploadsOriginalDir . $image->getOriginalUrl();
        }
        exec('perl ' . $this->kernelRootDir . '/exif.pl ' . $filename, $result);

        $formattedResult = [];
        foreach ($result as $item) {
            $arr = explode(' : ', $item, 2);
            if (count($arr) >= 2) {
                $formattedResult[$arr[0]] = $arr[1];
            } elseif (count($arr) == 1) {
                $formattedResult[$arr[0]] = '';
            }
        }
        ksort($formattedResult);

        return $formattedResult;
    }
}
