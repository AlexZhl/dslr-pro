<?php

namespace AppBundle\Service;

use AppBundle\Entity\Image;
use Doctrine\Bundle\DoctrineBundle\Registry;

class ImageUploader
{
    public $allowed_filetypes = 'png|jpg|jpeg|tif|nef|cr2|arw|raf|rw2|dng|pef|x3f';
    public $formats_for_convert = 'nef|cr2|arw|raf|rw2|dng|pef|x3f';
    public $uploadsDir;
    public $uploadsOriginalDir;

    public function __construct($uploads_dir, $uploads_original_dir)
    {
        $this->uploadsDir = $uploads_dir;
        $this->uploadsOriginalDir = $uploads_original_dir;

        $this->allowed_filetypes   = explode('|', $this->allowed_filetypes);
        $this->formats_for_convert = explode('|', $this->formats_for_convert);
    }

    public function uploadImage($uploadedBy, $filename = 'pic')
    {
        if (!(isset($_FILES[$filename]['tmp_name']) && isset($_FILES[$filename]['name']))) {
            $uploaderData['error'] = true;
            $uploaderData['message'] = 'Error uploading file to the server';
            return $uploaderData;
        }
        $file = $_FILES[$filename];
        $extension = strtolower(pathinfo($file['name'], PATHINFO_EXTENSION));
        $uploaderData = [
            'error' => false,
            'message' => '',
            'originalFilename' => $file['name'],
            'url' => '',
            'originalUrl' => '',
        ];

        if (!$this->checkIsAllowedFiletype($extension)) {
            $uploaderData['error'] = true;
            $uploaderData['message'] = 'File type is not supported';
            return $uploaderData;
        }

        $uniqueFilename = md5(uniqid());
        if ($this->checkIsConvertNeeded($extension)) {

            $originalFilePath = $this->uploadsOriginalDir . $uniqueFilename . '.' . $extension;
            if (!move_uploaded_file($file['tmp_name'], $originalFilePath)) {
                $uploaderData['error'] = true;
                $uploaderData['message'] = 'Can\'t move file';
                return $uploaderData;
            }
            $filePath = $this->uploadsDir . $uniqueFilename . '.jpg';

            $exec_result = '';
            $output_var = '';
            exec(sprintf('convert %s %s', $originalFilePath, $filePath), $exec_result, $output_var);

            if ($output_var) {
                $lostOriginalFilePath = $this->uploadsOriginalDir . 'lost_' . $uniqueFilename . '.' . $extension;
                rename($originalFilePath, $lostOriginalFilePath);

                $uploaderData['error'] = true;
                $uploaderData['message'] = 'Error occured. Unable to convert file';
                return $uploaderData;
            }

            // If everything is OK
            $uploaderData['url'] = $uniqueFilename . '.jpg';
            $uploaderData['originalUrl'] = $uniqueFilename . '.' . $extension;
            return $uploaderData;
        }

        $filePath = $this->uploadsDir . $uniqueFilename . '.' . $extension;
        if (!move_uploaded_file($file['tmp_name'], $filePath)) {
            $uploaderData['error'] = true;
            $uploaderData['message'] = 'Can\'t move file';
            return $uploaderData;
        }
        // If everything is OK
        $uploaderData['url'] = $uniqueFilename . '.' . $extension;
        return $uploaderData;
    }

    public function checkIsAllowedFiletype($extension)
    {
        return in_array($extension, $this->allowed_filetypes, true);
    }

    public function checkIsConvertNeeded($extension)
    {
        return in_array($extension, $this->formats_for_convert, true);
    }
}