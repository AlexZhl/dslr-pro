<?php

namespace AppBundle\Service;

use AppBundle\Entity\Image;

class ImageExifMailer
{
    public $swift_mailer;

    public $mailer_transport;
    public $mailer_host;
    public $mailer_user;
    public $mailer_password;
    public $mailer_from;
    public $templating;

    public function __construct(\Swift_Mailer $swift_Mailer, $mailer_transport, $mailer_host, $mailer_user, $mailer_password, $mailer_from, $templating)
    {
        $this->swift_mailer = $swift_Mailer;
        $this->mailer_transport;
        $this->mailer_host = $mailer_host;
        $this->mailer_user = $mailer_user;
        $this->mailer_password = $mailer_password;
        $this->mailer_from = $mailer_from;
        $this->templating = $templating;
    }

    public function sendExif(Image $image, $exif, $email)
    {
        $message = \Swift_Message::newInstance()
            ->setSubject('Image exif')
            ->setFrom($this->mailer_user, $this->mailer_from)
            ->setTo($email)
            ->setBody(
                $this->templating->render('email/exif.html.twig', [
                    'exif' => $exif,
                    'image' => $image,
                ]),
                'text/html'
            );

        $this->swift_mailer->send($message);
    }
}