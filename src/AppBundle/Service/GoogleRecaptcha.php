<?php

namespace AppBundle\Service;

use Symfony\Component\HttpFoundation\Request;

class GoogleRecaptcha
{
    public $recaptchaSecret;

    public function __construct($recaptcha_secret)
    {
        $this->recaptchaSecret = $recaptcha_secret;
    }

    public function validate(Request $request)
    {
        $recaptchaResponse = $request->request->get('g-recaptcha-response');

        $verifyResponse = file_get_contents(
            'https://www.google.com/recaptcha/api/siteverify?secret=' . $this->recaptchaSecret .
            '&response=' .$recaptchaResponse);
        $responseData = json_decode($verifyResponse);

        if(isset($responseData->success) && $responseData->success) {
            return true;
        }
        return false;
    }
}