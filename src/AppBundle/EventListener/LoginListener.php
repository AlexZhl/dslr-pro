<?php

namespace AppBundle\EventListener;

use AppBundle\Entity\Image;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use FOS\UserBundle\Controller\RegistrationController as BaseController;
use Symfony\Component\HttpFoundation\Request;
use FOS\UserBundle\Event\FilterUserResponseEvent;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\Form\Factory\FactoryInterface;
use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Model\UserManagerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Http\SecurityEvents;

class LoginListener implements EventSubscriberInterface
{
    private $container;

    public function __construct($container)
    {
        $this->container = $container;
    }

    public static function getSubscribedEvents()
    {
        return array(
            FOSUserEvents::SECURITY_IMPLICIT_LOGIN => 'onLogin',
            SecurityEvents::INTERACTIVE_LOGIN => 'onLogin',
        );
    }

    public function onLogin($event)
    {
        $imageId = $this->container->get('session')->get('imageId', 0);
        if ($imageId) {
            $this
                ->container
                ->get('event_dispatcher')
                ->addListener(KernelEvents::RESPONSE, [$this, 'onKernelResponse']);
        }
    }

    public function onKernelResponse($event)
    {
        $imageId = $this->container->get('session')->get('imageId', 0);
        if ($imageId) {
            $this->container->get('session')->remove('imageId');

            $user =  $this->container->get('security.token_storage')->getToken()->getUser();

            $image = $this->container->get('doctrine')->getRepository(Image::class)->find($imageId);
            $image->setUser($user);
            $this->container->get('doctrine.orm.entity_manager')->flush($image);

            $url = $this->container->get('router')->generate('app.account.image.imageEdit', [
                'id' => $image->getId(),
                'showExif' => true,
            ]);

            $event->getResponse()->headers->set('Location', $url);
        }
    }
}