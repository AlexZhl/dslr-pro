<?php

namespace AppBundle\Controller\Admin;

use AppBundle\Entity\Image;
use AppBundle\Entity\User;
use AppBundle\Form\ImageEditType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ImageController extends Controller
{
    const PAGE_NUMBER = 1;
    const ITEMS_PER_PAGE = 10;

    /**
     * @Route("/admin/images/", name="app.admin.image.list")
     */
    public function listAction(Request $request)
    {
        $page = $request->query->getInt('page', self::PAGE_NUMBER);
        $itemsPerPage = $request->query->getInt('itemsPerPage', self::ITEMS_PER_PAGE);
        $paginator = $this->get('knp_paginator');

        $pagination = $paginator
            ->paginate(
                $this->getDoctrine()->getRepository(Image::class)->getQueryImagesAll(),
                $page,
                $itemsPerPage
            );

        return $this->render('admin/image/list.html.twig', [
            'pagination' => $pagination,
        ]);
    }

    /**
     * @Route("/admin/images/{id}/edit/", name="app.admin.image.edit")
     */
    public function editAction(Request $request, $id)
    {
        $image = $this->getDoctrine()->getRepository(Image::class)->find($id);
        $user = $image->getUser();
        $form = $this->createForm(ImageEditType::class, $image);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->get('doctrine.orm.entity_manager');
            $em->flush();
            $this->addFlash('notice', 'Die Datei erfolgreich bearbeitet');
            return $this->redirectToRoute('app.admin.image.list');
        }

        return $this->render('admin/image/edit.html.twig', [
            'uploadedBy' => $user,
            'image' => $image,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/admin/images/{id}/delete/", name="app.admin.image.delete")
     */
    public function deleteAction(Request $request, $id)
    {
        $imageRepo = $this->getDoctrine()->getRepository(Image::class);
        $image = $imageRepo->find($id);

        if ($request->query->getBoolean('confirm', false)) {
            $imageRepo->deleteImageWithLikes($id, $this->getParameter('uploads_dir'), $this->getParameter('uploads_original_dir'));
            $this->addFlash('error', 'Image has been deleted');
        } else {
            return $this->render('admin/image/confirmDelete.html.twig', [
                'image' => $image,
            ]);
        }

        return $this->redirectToRoute('app.admin.image.list');
    }
}
