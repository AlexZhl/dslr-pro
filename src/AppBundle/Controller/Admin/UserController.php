<?php

namespace AppBundle\Controller\Admin;

use AppBundle\Entity\Image;
use AppBundle\Entity\User;
use AppBundle\Form\EditUserType;
use AppBundle\Form\ProfileSettingsType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class UserController extends Controller
{
    const PAGE_NUMBER = 1;
    const ITEMS_PER_PAGE = 10;

    /**
     * @Route("/admin/users/", name="app.admin.user.list")
     */
    public function listAction(Request $request)
    {
        $page = $request->query->getInt('page', self::PAGE_NUMBER);
        $itemsPerPage = $request->query->getInt('itemsPerPage', self::ITEMS_PER_PAGE);

        $paginator = $this->get('knp_paginator');
        $query = $this->getDoctrine()->getRepository(User::class)->getQueryUsersAll();
        $pagination = $paginator->paginate($query, $page, $itemsPerPage);

        return $this->render('admin/user/list.html.twig', [
            'pagination' => $pagination,
        ]);
    }

    /**
     * @Route("/admin/users/{id}/lock/", name="app.admin.user.lock")
     */
    public function lockAction(Request $request, $id)
    {
        $um = $this->get('fos_user.user_manager');
        $user = $um->findUserBy([
            'id' => $id,
        ]);
        if ($user->isAccountNonLocked()) {
            $user->setLocked(true);
            $this->addFlash('error', 'User ' . $user->getUsername() . ' has been locked');
        } else {
            $user->setLocked(false);
            $this->addFlash('notice', 'User ' . $user->getUsername() . ' has been unlocked');
        }
        $um->updateUser($user);

        return $this->redirectToRoute('app.admin.user.list');
    }

    /**
     * @Route("/admin/users/{id}/delete/", name="app.admin.user.delete")
     */
    public function deleteAction(Request $request, $id)
    {
        $um = $this->get('fos_user.user_manager');
        $user = $um->findUserBy([
            'id' => $id,
        ]);

        if ($request->query->getBoolean('confirm', false)) {
            $this->addFlash('error', 'User ' . $user->getUsername() . ' has been deleted');
            $um->deleteUser($user);
        } else {
            return $this->render('admin/user/confirmDelete.html.twig', [
                'user' => $user,
            ]);
        }

        return $this->redirectToRoute('app.admin.user.list');
    }

    /**
     * @Route("/admin/users/{id}/edit/", name="app.admin.user.edit")
     */
    public function editAction(Request $request, $id)
    {
        $um = $this->get('fos_user.user_manager');
        $user = $um->findUserBy([
            'id' => $id,
        ]);
        $form = $this->createForm(EditUserType::class, $user);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $um = $this->get('fos_user.user_manager');
            $um->updateUser($user);
            $this->addFlash('notice', ('User saved'));
        }

        return $this->render('admin/user/edit.html.twig', [
            'user' => $user,
            'roles' => $user->getRoles(),
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/admin/users/{id}/promote/", name="app.admin.user.promote")
     */
    public function promoteAction(Request $request, $id)
    {
        $promotionRole = $request->query->get('role');
        if (!$promotionRole) {
            return $this->redirectToRoute('app.admin.user.edit', [ 'id' => $id ]);
        }
        if (!$this->getUser()->hasRole('ROLE_SUPER_ADMIN')) {
            $this->addFlash('error', 'Access denied');
            return $this->redirectToRoute('app.admin.user.list');
        }

        $um = $this->get('fos_user.user_manager');
        $user = $um->findUserBy([ 'id' => $id ]);
        if ($user->hasRole($promotionRole)) {
            $user->removeRole($promotionRole);
            $um->updateUser($user);
            $this->addFlash('error', 'Role ' . $promotionRole . ' removed');
            return $this->redirectToRoute('app.admin.user.edit', [ 'id' => $id ]);
        } else {
            $user->addRole($promotionRole);
            $um->updateUser($user);
            $this->addFlash('notice', 'Role ' . $promotionRole . ' added');
            return $this->redirectToRoute('app.admin.user.edit', [ 'id' => $id ]);
        }

        return $this->redirectToRoute('app.admin.user.list');
    }
}
