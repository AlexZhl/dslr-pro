<?php

namespace AppBundle\Controller\Account;

use AppBundle\Entity\Image;
use AppBundle\Form\ImageEditType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ImageController extends Controller
{
    /**
     * @Route("/profile/images/", name="app.account.image.userImages")
     */
    public function userImagesAction(Request $request)
    {
        $page = $request->query->getInt('page', 1);
        $itemsPerPage = $request->query->getInt('itemsPerPage', Image::ITEMS_PER_PAGE);
        $paginator = $this->get('knp_paginator');
        $query = $this
            ->getDoctrine()
            ->getRepository(Image::class)
            ->getQueryImagesUploadedByUser($this->getUser());
        $pagination = $paginator->paginate($query, $page, $itemsPerPage);

        return $this->render('account/userImages.html.twig', [
            'pagination' => $pagination
        ]);
    }

    /**
     * @Route("/profile/images/{id}/", name="app.account.image.imageDetails")
     */
    public function imageDetailsAction(Request $request, $id)
    {
        $image = $this->getDoctrine()->getRepository(Image::class)->isOwner($id, $this->getUser());

        if (!$image) {
            $this->addFlash('error', 'Access denied');
            return $this->redirectToRoute('app.account.user.profile');
        }

        return $this->render('account/imageDetails.html.twig', [
            'image' => $image
        ]);
    }

    /**
     * @Route("/profile/images/{id}/edit/", name="app.account.image.imageEdit")
     */
    public function imageEditAction(Request $request, $id)
    {
        $image = $this
            ->getDoctrine()
            ->getRepository(Image::class)
            ->isOwner($id, $this->getUser());
        if (!$image) {
            $this->addFlash('error', 'Access denied');
            return $this->redirectToRoute('app.account.user.profile');
        }

        $form = $this->createForm(ImageEditType::class, $image);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->get('doctrine.orm.entity_manager');
            $em->flush();
            $this->addFlash('notice', 'Die Datei erfolgreich bearbeitet');
            return $this->redirectToRoute('app.account.image.userImages');
        }

        $exif = null;
        if ($request->query->getBoolean('showExif')) {
            $exif = $this->get('image.exif.tool')->getExif($image);
        }

        return $this->render('account/imageEdit.html.twig', [
            'image' => $image,
            'form' => $form->createView(),
            'exif' => $exif,
        ]);
    }

    /**
     * @Route("/profile/images/{id}/exif/", name="app.account.image.imageExif")
     */
    public function imageExifAction(Request $request, $id)
    {
        $image = $this->getDoctrine()->getRepository(Image::class)->isOwner($id, $this->getUser());

        if (!$image) {
            $this->addFlash('error', 'Access denied');
            return $this->redirectToRoute('app.account.user.profile');
        }
        $exif = $this->get('image.exif.tool')->getExif($image);

        return $this->render('account/imageExif.html.twig', [
            'image' => $image,
            'exif' => $exif,
        ]);
    }

    /**
     * @Route("/profile/images/{id}/exif/email", name="app.account.image.emailExif")
     */
    public function emailExifAction(Request $request, $id)
    {
        $image = $this->getDoctrine()->getRepository(Image::class)->isOwner($id, $this->getUser());

        if (!$image) {
            $this->addFlash('error', 'Access denied');
            return $this->redirectToRoute('app.account.user.profile');
        }
        $exif = $this->get('image.exif.tool')->getExif($image);

        if ($request->query->getBoolean('send')) {
            $this->get('image.exif.mailer')->sendExif($image, $exif, $this->getUser()->getEmail());
            $this->addFlash('notice', 'Email wurde versendet');
            return $this->redirectToRoute('app.account.image.imageDetails', ['id' => $id]);
        }

        return $this->render('account/emailMeExif.html.twig', [
            'sender' => $this->getParameter('mailer_from'),
            'image' => $image,
            'exif' => $exif,
            'user' => $this->getUser(),
        ]);
    }

    /**
     * @Route("/profile/images/{id}/delete/", name="app.account.image.delete")
     */
    public function deleteAction(Request $request, $id)
    {
        $image = $this->getDoctrine()->getRepository(Image::class)->isOwner($id, $this->getUser());

        if (!$image) {
            $this->addFlash('error', 'Access denied');
            return $this->redirectToRoute('app.account.user.profile');
        }

        $this->getDoctrine()
            ->getRepository(Image::class)
            ->deleteImageWithLikes(
                $id,
                $this->getParameter('uploads_dir'),
                $this->getParameter('uploads_original_dir')
            );
        $this->addFlash('error', 'Image has been deleted');

        return $this->redirectToRoute('app.account.image.userImages');
    }
}
