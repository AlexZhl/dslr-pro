<?php

namespace AppBundle\Controller\Account;

use AppBundle\Entity\Image;
use AppBundle\Entity\User;
use AppBundle\Form\ProfileSettingsType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class UserController extends Controller
{
    /**
     * @Route("/profile/", name="app.account.user.profile")
     */
    public function profileAction(Request $request)
    {
        $user = $this->getUser();
        return $this->render('account/profile.html.twig', [
            'user' => $user,
        ]);
    }

    /**
     * @Route("/profile/settings/", name="app.account.user.settings")
     */
    public function settingsAction(Request $request)
    {
        $user = $this->getUser();
        $form = $this->createForm(ProfileSettingsType::class, $user);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $um = $this->get('fos_user.user_manager');
            $um->updateUser($user);
            $this->addFlash('notice', ('Settings saved'));
        }

        return $this->render('account/settings.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/profile/fos/", name="fos_user_profile_show")
     */
    public function redirectAction(Request $request)
    {
        return $this->redirectToRoute('app.account.user.settings');
    }
}
