<?php

namespace AppBundle\Controller\Home;

use AppBundle\Entity\Image;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends Controller
{
    /**
     * @Route("/", name="app.home.home.index")
     */
    public function indexAction(Request $request)
    {
        $page = $request->query->getInt('page', 1);
        $itemsPerPage = $request->query->getInt('itemsPerPage', Image::ITEMS_PER_PAGE);
        $paginator = $this->get('knp_paginator');
        $query = $this
            ->getDoctrine()
            ->getRepository(Image::class)
            ->getQueryImagesSortedByRating();
        $pagination = $paginator->paginate($query, $page, $itemsPerPage);

        return $this->render('home/index.html.twig', [
            'pagination' => $pagination,
            'user' => $this->getUser(),
        ]);
    }

    /**
     * @Route("/about/", name="app.home.home.about")
     */
    public function aboutAction(Request $request)
    {
        return $this->render('home/about.html.twig', []);
    }

    /**
     * @Route("/contact/", name="app.home.home.contact")
     */
    public function contactAction(Request $request)
    {
        return $this->render('home/contact.html.twig', []);
    }

    /**
     * @Route("/license/", name="app.home.home.license")
     */
    public function licenseAction(Request $request)
    {
        return $this->render('home/license.html.twig', []);
    }
}
