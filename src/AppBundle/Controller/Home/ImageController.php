<?php

namespace AppBundle\Controller\Home;

use AppBundle\Entity\Image;
use AppBundle\Entity\Rating;
use AppBundle\Entity\User;
use FOS\UserBundle\Form\Type\UsernameFormType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ImageController extends Controller
{
    /**
     * @Route("/image/{id}/", name="app.home.image.imageDetails")
     */
    public function imageDetailsAction(Request $request, $id)
    {
        $image = $this->getDoctrine()->getRepository(Image::class)->find($id);

        if (!$image) {
            $this->addFlash('error', 'Error');
            return $this->redirectToRoute('app.home.home.index');
        }
        if (!$image->getIsPublic()) {
            if (!($image->getUser() == $this->getUser())) {
                $this->addFlash('error', 'Error');
                return $this->redirectToRoute('app.home.home.index');
            }
        }

        $exif = $this->get('image.exif.tool')->getExif($image);
        return $this->render('home/imageDetails.html.twig', [
            'image' => $image,
            'exif' => $exif,
        ]);
    }

    /**
     * @Route("/image/{id}/exif/email", name="app.home.image.emailExif")
     */
    public function emailExifAction(Request $request, $id)
    {
        if (!$this->getUser()) {
            return $this->redirectToRoute('app.account.user.profile');
        }

        $image = $this->getDoctrine()->getRepository(Image::class)->find($id);

        if (!$image) {
            $this->addFlash('error', 'Error');
            return $this->redirectToRoute('app.home.home.index');
        }
        if (!$image->getIsPublic()) {
            if (!($image->getUser() == $this->getUser())) {
                $this->addFlash('error', 'Error');
                return $this->redirectToRoute('app.home.home.index');
            }
        }

        $exif = $this->get('image.exif.tool')->getExif($image);

        if ($request->query->getBoolean('send')) {
            $this->get('image.exif.mailer')->sendExif($image, $exif, $this->getUser()->getEmail());
            $this->addFlash('notice', 'Email wurde versendet');
            return $this->redirectToRoute('app.home.image.imageDetails', ['id' => $id]);
        }

        return $this->render('home/emailMeExif.html.twig', [
            'sender' => $this->getParameter('mailer_from'),
            'image' => $image,
            'exif' => $exif,
            'user' => $this->getUser(),
        ]);
    }

    /**
     * @Route("/upload", name="app.home.image.uploadImage")
     * @Method("POST")
     */
    public function uploadImageAction(Request $request)
    {
        $response = [
            'httpStatus' => 0,
            'isError' => false,
            'message' => '',
            'redirectUrl' => 'http://',
            'html' => '',
        ];

        if (isset($_FILES['pic'])) {
            $uploaderData = $this->get('image.uploader')->uploadImage($this->getUser());

            if ($uploaderData['error']) {
                $response['httpStatus'] = 500;
                $response['isError'] = true;
                $response['message'] = $uploaderData['message'];
                return new JsonResponse($response);
            }

            $image = new Image($uploaderData['url']);
            $image->setOriginalUrl($uploaderData['originalUrl']);
            $image->setOriginalFilename($uploaderData['originalFilename']);

            if (!$this->isGranted('ROLE_USER')) {
                $this->addFlash('error', 'security.login.login_for_upload');

                $response['redirectUrl'] = $this->get('router')->generate('app.home.image.filesCatched');
                $response['httpStatus'] = 401;
                $response['isError'] = true;
                $response['message'] = 'Bitte anmelden';

                $this->getDoctrine()->getManager()->persist($image);
                $this->getDoctrine()->getManager()->flush($image);

                $this->get('session')->set('imageId', $image->getId());

                return new JsonResponse($response);
            }

            $image->setUser($this->getUser());

            $this->getDoctrine()->getManager()->persist($image);
            $this->getDoctrine()->getManager()->flush($image);

            $response['httpStatus'] = 301;
            $response['redirectUrl'] = $this->get('router')->generate('app.account.image.imageEdit', [
                'id' => $image->getId(),
                'showExif' => true,
            ]);
            $this->addFlash('notice', 'Die Datei ist erfolgreich hochgeladen.');
            $response['message'] = 'Image uploaded';

            return new JsonResponse($response);
        }
        $response['httpStatus'] = 500;
        $response['isError'] = true;
        $response['message'] = 'Datei wurde nicht ausgewählt';
        return new JsonResponse($response);
    }

    /**
     * @Route("/login-or-register", name="app.home.image.filesCatched")
     */
    public function filesCatchedAction(Request $request)
    {
        if ($this->isGranted('ROLE_USER')) {
            return $this->redirectToRoute('app.home.home.index');
        }

        $csrfToken = $this->has('security.csrf.token_manager')
            ? $this->get('security.csrf.token_manager')->getToken('authenticate')->getValue()
            : null;
        $registrationForm = $this->get('fos_user.registration.form.factory')->createForm()->createView();

        $imageId = $this->get('session')->get('imageId', 0);
        $image = $this->getDoctrine()->getRepository(Image::class)->find($imageId);
        if ($image) {
            $exif = $this->get('image.exif.tool')->getExif($image);

            return $this->render('security/filesCatched.html.twig', [
                'image' => $image,
                'exif' => $exif,
                'csrf_token' => $csrfToken,
                'registrationForm' => $registrationForm,
            ]);
        }

        return $this->render('security/filesCatched.html.twig', [
            'csrf_token' => $csrfToken,
            'registrationForm' => $registrationForm,
        ]);
    }


    /**
     * @Route("/changeRating", name="app.home.image.changeRating")
     */
    public function changeRatingAction(Request $request)
    {
        $imageId = $request->query->getInt('imageId');
        $isLike = $request->query->getBoolean('isLike');
        $user = $this->getUser();

        if ($imageId && $user && ($isLike !== null)) {
            $response = $this->getDoctrine()->getRepository(Rating::class)->changeRating($imageId, $user, $isLike);
            if (!$response['isError']) {
                return new JsonResponse($response);
            }
        }

        if (!$user) {
            $this->addFlash('alert alert-danger', 'Bitte anmelden');
            return new Response('unauthorized', 401);
        }
        return new Response('bad request', 400);
    }
}
