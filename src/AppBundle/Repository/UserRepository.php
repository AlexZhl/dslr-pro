<?php

namespace AppBundle\Repository;

use AppBundle\Entity\User;
use \Doctrine\ORM\EntityRepository;

class UserRepository extends EntityRepository
{
    public function getQueryUsersAll()
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb
            ->select('u.id, u.firstName, u.lastName, u.username, u.email, u.locked, u.enabled, u.lastLogin')
            ->from(User::class, 'u')
            ->orderBy('u.id', 'ASC');
        return $qb->getQuery();
    }
}
