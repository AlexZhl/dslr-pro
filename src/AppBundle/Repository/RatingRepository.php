<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Image;
use AppBundle\Entity\Rating;
use \Doctrine\ORM\EntityRepository;

class RatingRepository extends EntityRepository
{
    public function changeRating($imageId, $user, $isLike)
    {
        $response = [
            'message' => '',
            'newRating' => 0,
            'isError' => false,
        ];

        $image = $this->_em->getRepository(Image::class)->find($imageId);
        if (!$image) {
            $response['message'] = 'Image not found';
            $response['isError'] = true;
            return $response;
        }

        $rating = $this->findOneBy(['image' => $image, 'user' => $user]);
        if ($rating) {
            if ($rating->getVote() == $isLike) {
                if ($isLike) {
                    $image->setRating($image->getRating()-1);
                } else {
                    $image->setRating($image->getRating()+1);
                }
                $this->_em->remove($rating);
                $this->_em->flush();

                $response['message'] = 'You already ' . ($isLike ? 'like' : 'dislike') . ' this image';
                $response['newRating'] = $image->getRating();
                return $response;
            }
            if ($isLike == true) {
                $image->setRating($image->getRating()+2);
            } else {
                $image->setRating($image->getRating()-2);
            }
            $rating->setVote($isLike);
            $this->_em->flush();

            $response['message'] = 'Success! You changed ' . ($isLike ? 'dislike to like' : 'like to dislike') .
                ' on image ' . $imageId;
            $response['newRating'] = $image->getRating();
            return $response;
        } else {
            $rating = new Rating();
            $rating->setUser($user);
            $rating->setImage($image);
            if ($isLike == true) {
                $image->setRating($image->getRating()+1);
            } else {
                $image->setRating($image->getRating()-1);
            }
            $rating->setVote($isLike);
            $this->_em->persist($rating);
            $this->_em->flush();

            $response['message'] = 'Success! You put ' . ($isLike ? 'like' : 'dislike') .
                ' on image ' . $imageId;
            $response['newRating'] = $image->getRating();
            return $response;
        }

        $response['message'] = 'Uncaught error';
        $response['isError'] = true;
        return $response;
    }
}
