<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Image;
use AppBundle\Entity\Rating;
use AppBundle\Entity\User;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Validator\Constraints\DateTime;

class ImageRepository extends EntityRepository
{
    public function getQueryImagesUploadedByUser(User $user)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb
            ->select('i')
            ->from(Image::class, 'i')
            ->join(User::class, 'u')
            ->where('i.user = ?1')
            ->orderBy('i.uploadedAt', 'DESC');
        $qb->setParameter(1, $user);
        return $qb->getQuery();
    }

    public function getQueryImagesSortedByRating()
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb
            ->select('i')
            ->from(Image::class, 'i')
            ->orderBy('i.rating', 'DESC')
            ->where('i.isPublic = true');
        return $qb->getQuery();
    }

    public function isOwner($imageId, User $user)
    {
        $image = $this->find($imageId);
        if ($image) {
            if ($image->getUser()->getId() == $user->getId()) {
                return $image;
            } else {
                return false;
            }
        }
        return null;
    }

    public function getQueryImagesAll()
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb
            ->select('i.url, i.id, i.title, u.username, i.originalUrl, i.originalFilename, i.uploadedAt')
            ->from(Image::class, 'i')
            ->join(User::class, 'u', 'WITH', 'i.user = u.id')
            ->orderBy('i.uploadedAt', 'DESC');
        return $qb->getQuery();
    }

    public function deleteImageWithLikes($id, $uploads_dir, $uploads_original_dir)
    {
        $image = $this->find($id);

        if ($image->getUrl()) {
            unlink($uploads_dir . $image->getUrl());
        }
        if ($image->getOriginalUrl()) {
            unlink($uploads_original_dir . $image->getOriginalUrl());
        }

        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb
            ->delete(Rating::class, 'r')
            ->where('r.image = ?1');
        $qb->setParameter(1, $id);
        $qb->getQuery()->getResult();

        $em = $this->getEntityManager();
        $em->remove($image);
        $em->flush();
    }

    public function getOutdatedImages($sec)
    {
        $now = new \DateTime();
        $outdatedAt = new \DateTime();
        $outdatedAt->setTimestamp($now->getTimestamp() - $sec);

        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb
            ->select('i')
            ->from(Image::class, 'i')
            ->where('i.user is NULL')
            ->andWhere('i.uploadedAt <= :outdated')
            ->setParameter('outdated', $outdatedAt);
        return $qb->getQuery()->getResult();
    }
}
